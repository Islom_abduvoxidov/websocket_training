import "./App.css";
import SiderDemo from "./pages/main";

function App() {
  return (
    <div className="App">
      <SiderDemo />
    </div>
  );
}

export default App;
